import gitlab
import os

URL="https://gitlab.com/"
PRIVATE_ACCESS_TOKEN = os.environ['PRIVATE_ACCESS_TOKEN']

gl = gitlab.Gitlab(URL, private_token=PRIVATE_ACCESS_TOKEN,)

gl.auth()

username = "mmoumni2"
project_name = "gitlab-api"

project = gl.projects.get(f"{username}/{project_name}")
# print(project.get_id())

# get all owned projects
# projects = gl.projects.list(get_all=False,owned=True)

# for project in projects:
#     print(f"{project.id} {project.name}")

# get the branches of a project
branches = project.branches.list()

print(branches)
